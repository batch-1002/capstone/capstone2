const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name required']
	},
	lastName: {
		type: String,
		required: [true, 'Last name required']
	},
	password: {
		type: String,
		required: [true, 'Password required']
	},
	email: {
		type: String,
		required: [true, 'Email required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Enrolled' // Other values: Cancelled and Completed
			}
		}
	]
});

module.exports = mongoose.model('User', userSchema);