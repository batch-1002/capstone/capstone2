const bcrypt = require('bcrypt');
const User = require('../models/User');
const Course = require('../models/Course');
const auth = require('../auth');

module.exports.register = async (params) => {
	const validPassword = params.password.length >= 6;
	const mobileNo = params.mobileNo.length === 11;

	if (!validPassword) {
		// return "Password should be at least 6 characters long";
		return false
	} else if(!mobileNo) {
		// return "Mobile Number should be 11 digits long";
		return false
	}

	let emailExists = !!(await User.findOne({ email: params.email }));

	// if (emailExists) return "Email already exists, choose a new one";
	if (emailExists) return false;

	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
		mobileNo: params.mobileNo
	});


	return user.save().then((user, err) => (err) ? false : true);
}

module.exports.login = async (params) => {
	let user = await User.findOne({ email: params.email });
	if (user === null) return false;

	let isPasswordCorrect = bcrypt.compareSync(params.password, user.password);

	if (isPasswordCorrect) {
		return { accessToken: auth.createAccessToken(user.toObject()) }
	} else {
		return false;
	}
}

module.exports.getDetails = async (params) => {
	let user = await User.findById(params.userId);
	if (user === null) return false;
	return {
		firstName: user.firstName,
		lastName: user.lastName,
		email: user.email,
		enrollements: user.enrollements,
		isAdmin: user.isAdmin,
		mobileNo: user.mobileNo
	};
}

module.exports.enroll = async (params) => {
	let user = await User.findById(params.userId);
	let course = await Course.findById(params.courseId);

	if (user === null || course === null || !course.isActive) return false;

	let isUserEnrolled = 
		!!(user.enrollments.find(course => course.courseId === params.courseId)) ||
		!!(course.enrollees.find(user => user.userId === params.userId));

	if (isUserEnrolled) return false;

	user.enrollments.push({ courseId: params.courseId });
	course.enrollees.push({ userId: params.userId });

	try {
		await user.save();
		await course.save();
		return true;
	} catch (err) {
		return false;
	}
}

module.exports.getCourses = async (params) => {
	try {
		let user = await User.findById(params.userId);
		let courses = await Promise.all(user.enrollments.map(async enrollment => {
			let course =  await Course.findById(enrollment.courseId);
			return course.name;
		}));
		return courses;
	} catch (err) {
		console.error(err);
		return false;
	}
}


