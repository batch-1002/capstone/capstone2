const Course = require('../models/Course');
const User = require('../models/User');

module.exports.create = async (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price,
	});

	let isExistingCourse = await Course.findOne({ name: params.name });
	if (isExistingCourse) return false;

	try {
		await course.save();
		return true;
	} catch (err) {
		return false;
	}
}

module.exports.getAllActive = async (params) => {
	try {
		let courses = await Course.find({ isActive: true }); 
		return (courses.map(course => {
			course = course.toObject(); // convert to JS object to add isEnrolled
			course.isEnrolled = !!(course.enrollees
				.find(enrollee => enrollee.userId === params.userId));
			return course;
		}));
	} catch (err) {
		return false;
	}
}

module.exports.getAll = async () => {
	try {
		return await Course.find(); 
	} catch (err) {
		return false;
	}	
}

module.exports.get = async (params) => {
	try {
		let course = await Course.findById(params.courseId); 
		if (course.isActive === false) return false;
		course = course.toObject();
		course.isEnrolled = !!(course.enrollees
			.find(enrollee => enrollee.userId === params.userId)
		);
		return course;
	} catch (err) {
		console.error(err);
		return false;
	}	
}

module.exports.forceGet = async (params) => {
	try {
		return await Course.findById(params.courseId); 
	} catch (err) {
		return false;
	}	
}

module.exports.deactivate = async (params) => {
	try {
		await Course.findByIdAndUpdate(params.courseId, { isActive: false });
		return true;
	} catch (err) {
		return false;
	}
}

module.exports.activate = async (params) => {
	try {
		await Course.findByIdAndUpdate(params.courseId, { isActive: true });
		return true;
	} catch (err) {
		return false;
	}
}

module.exports.update = async (params) => {
	try {
		let editedCourse = {
			name: params.name,
			description: params.description,
			price: params.price
		}

		await Course.findByIdAndUpdate(params.courseId, editedCourse);
		return true;
	} catch (err) {
		return false;
	}
}

module.exports.getEnrollees = async (params) => {
	try {
		let course = await Course.findById(params.courseId); 
		return await Promise.all(course.enrollees.map(async enrollee => {
			let user = await User.findById(enrollee.userId);
			return user.email;
		}));
	} catch (err) {
		return false;
		console.error(err);
	}
}