const jwt = require('jsonwebtoken');
require('dotenv').config();
const secret = process.env.SECRET;

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	token = req.headers.authorization;

	if (typeof token !== 'undefined') {
		token = token.slice(7);

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send({ auth: 'failed' }) : next();
		});
	} else {
		res.send({ auth: 'failed' });
	}
}

module.exports.adminVerify = async (req, res, next) => {
	token = req.headers.authorization;

	if (typeof token !== 'undefined') {
		token = token.slice(7);
		try {
			await jwt.verify(token, secret);
			let isAdmin = jwt.decode(token, {complete: true}).payload.isAdmin;
			(isAdmin) ? next() : res.send({auth: 'failed'});
		} catch(err) {
			return res.send({ auth: 'failed' });
		}
	} else {
		res.send({ auth: 'failed' });
	}
}

module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7);

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, { complete: true }).payload;
		});
	} else {
		return (null);
	}
}