const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();

const UserRouter = require('./routes/user');
const CourseRouter = require('./routes/course');

mongoose.connect(
	process.env.MONGODB_STRING,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false
	}
);
mongoose.connection.once('open', () => console.log('Connected to MongoDB Atlas'));

const port = process.env.PORT;
app.listen(port, () => console.log(`Listening on port ${port}`));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/users', UserRouter);
app.use('/api/courses', CourseRouter);