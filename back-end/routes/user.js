const router = require('express').Router();
const UserController = require('../controllers/user');
const auth = require('../auth');

router.post('/', (req, res) => {
	UserController.register(req.body).then(result => res.send(result));
});

router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result));
});

router.get('/details', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id
	}
	UserController.getDetails(params).then(result => res.send(result));
});

router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	UserController.enroll(params).then(result => res.send(result));
})

router.get('/courses', auth.verify, (req, res) => {
	const params = { userId: auth.decode(req.headers.authorization).id }
	UserController.getCourses(params).then(result => res.send(result));
})

module.exports = router;