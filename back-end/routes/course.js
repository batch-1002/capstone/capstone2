const router = require('express').Router();
const auth = require('../auth');
const CourseController = require('../controllers/course');

router.post('/', auth.adminVerify, (req, res) => {
	CourseController.create(req.body).then(result => res.send(result));
})

router.get('/', auth.verify, (req, res) => {
	// get only acitve courses
	CourseController.getAllActive({ userId: auth.decode(req.headers.authorization).id })
	.then(result => res.send(result));
})

router.get('/all', auth.adminVerify, (req, res) => {
	// get all courses, even inactive ones
	CourseController.getAll().then(result => res.send(result));
})

router.get('/:courseId', auth.verify, (req, res) => {
	// get details of active courses
	const userId = auth.decode(req.headers.authorization).id;
	CourseController.get({ courseId: req.params.courseId, userId })
		.then(result => res.send(result));
});

router.get('/:courseId/force-get', auth.adminVerify, (req, res) => {
	// get course even if its inactive
	CourseController.forceGet({ courseId: req.params.courseId })
		.then(result => res.send(result));
});

router.get('/enrollees/:courseId', auth.adminVerify, (req, res) => {
	CourseController.getEnrollees({ courseId: req.params.courseId })
		.then(result => res.send(result));
})

router.delete('/:courseId', auth.adminVerify, (req, res) => {
	CourseController.deactivate({ courseId: req.params.courseId })
		.then(result => res.send(result));
});

router.put('/:courseId/make-active', auth.adminVerify, (req, res) => {
	CourseController.activate({ courseId: req.params.courseId })
		.then(result => res.send(result));
});


router.put('/:courseId', auth.adminVerify, (req, res) => {
	req.body.courseId = req.params.courseId;
	console.log(req.body);
	CourseController.update(req.body).then(result => res.send(result));
});

module.exports = router;