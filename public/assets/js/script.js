//Add the logic that will show the login button when a user is not logged in


let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector('#registerBtn');
let profileBtn = document.querySelector('#profileBtn');
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem('isAdmin') === 'true';

let pathArray = (window.location.pathname.split('/'));
let isPages = !!(pathArray.find(path => path === 'pages'));

if(!userToken) {
	navItems.innerHTML = 
	`
		<li class="nav-item"> 
			<a class="nav-link"> Log in </a>
		</li>
	`
	registerBtn.innerHTML =
	`
		<li class="nav-item">
			<a class="nav-link"> Sign up </a>
		</li>
	`

	navItems.addEventListener('click', () => {
		(isPages)
			? window.location.replace('./login.html')
			: window.location.replace('./pages/login.html');
	});

	registerBtn.addEventListener('click', () => {
		(isPages)
			? window.location.replace('./register.html')
			: window.location.replace('./pages/register.html');
	})
} else {
	navItems.innerHTML = 
	`
		<li class="nav-item">
			<a class="nav-link"> Logout </a>
		</li>
	`

	navItems.addEventListener('click', () => {
		(isPages)
			? window.location.replace('./logout.html')
			: window.location.replace('./pages/logout.html');
	});


	if (!isAdmin) {
		profileBtn.innerHTML = 
		`
			<li class="nav-item">
				<a class="nav-link">Profile</a>
			</li>
		`

		profileBtn.addEventListener('click', () => {
			(isPages)
				? window.location.replace('./userProfile.html')
				: window.location.replace('./pages/userProfile.html');
		});
	}
}
