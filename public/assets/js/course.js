let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');


let courseName = document.querySelector('#courseName');
let courseDescription = document.querySelector('#courseDescription');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

const displayCourse = async () => {
	let courseResponse;

	if (isAdmin) {
		courseResponse = await fetch(`http://localhost:3000/api/courses/${courseId}/force-get`, {
			'headers': {
				'authorization': `bearer ${token}`
			}
		});
	} else {
		courseResponse = await fetch(`http://localhost:3000/api/courses/${courseId}`, {
			'headers': {
				'authorization': `bearer ${token}`
			}
		});
	}

	let courseData = await courseResponse.json();

	courseName.innerHTML = courseData.name;
	courseDescription.innerHTML = courseData.description;
	coursePrice.innerHTML = courseData.price;
	if (isAdmin) { //isAdmin comes script.js
		displayEnrollees();
	} else {
		initializeEnrollButton(courseData);
	}
}

const initializeEnrollButton = (courseData) => {
	if (courseData.isEnrolled) {
		enrollContainer.innerHTML = 
		`
		<button id="enrollButton" class="btn btn-block btn-secondary" disabled>
			Already Enroll
		</button>
		<a href="./courses.html" class="btn btn-block btn-primary">Back</a>
		`
	} else {
		enrollContainer.innerHTML = 
		`
		<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>
		<a href="./courses.html" class="btn btn-block btn-primary">Back</a>
		`
	}
	

	let enrollButton = document.querySelector('#enrollButton');
	enrollButton.addEventListener('click', async () => {
		try {
			let enrollResponse = await fetch(`http://localhost:3000/api/users/enroll`, {
				'method': 'POST',
				'headers': {
					'authorization': `bearer ${token}`,
					'Content-Type': 'application/json'
				},
				'body': JSON.stringify({
					courseId
				})
			});
			let enrollData = await enrollResponse.json();
			if (enrollData === true) {
				alert('Enrolled Successfully');
			} else {
				alert('Already enrolled')
			}
			window.location.replace('./courses.html');
		} catch(err) {
			console.error(err);
			alert('Something went wrong, refresh the page and try again');
		}
	});
}

const displayEnrollees = async () => {
	// fetch enrollees
	// place in a h1 and p
	// place in innerhtml of enrollment container
	let enrolleesResponse = await fetch(
		`http://localhost:3000/api/courses/enrollees/${courseId}`,
		{
			'headers': {
				'Authorization': `bearer ${token}`
			}
		}
	);

	let enrolleesData = await enrolleesResponse.json();
	console.log(enrolleesData);

	let enrolleesDisplay;

	if (enrolleesData.length < 1) {
		enrolleesDisplay = '<p>No one is currently enrolled</p>'
	} else {
		enrolleesDisplay = enrolleesData.map((enrollees, index) => {
			return (
				`
					<p>${index + 1}: ${enrollees}</p>
				`
			);
		}).join('');
	}

	enrollContainer.innerHTML = `
		<h3>Currently enrolled:</h3>
		${enrolleesDisplay}
		<a href="./courses.html" class="btn btn-primary">Back</a>
	`
}

displayCourse();
