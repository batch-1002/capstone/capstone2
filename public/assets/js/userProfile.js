const token = localStorage.getItem('token');

// create async function that when called will:
//fetch user details
// then fetch courses
//fetch names of courses

const getEnrolledCourses = async () => {
	try {
		let enrolledCoursesResponse = await fetch('http://localhost:3000/api/users/courses', {
			'headers': {
				'authorization': `bearer ${token}`
			}
		});
		let enrolledCoursesData = await enrolledCoursesResponse.json();

		if (enrolledCoursesData.length < 1) return '<h2>Enrolled Courses</h2><p>None</p>'

		let coursesHTML = enrolledCoursesData.map((course, index) => {
			return (
				`
					<p>${index + 1}. ${course}</p>
				`
			)
		}).join('');

		return (
			`
				<h2>Enrolled Courses</h2>
				${coursesHTML}
			`
		);
	} catch(err) {
		console.error(err);
		alert('Something went wrong, please refresh the page and try again')
	}
}

const getUserProfileInfo = async () => {
	try {
		let userResponse = await fetch('http://localhost:3000/api/users/details', {
			'headers': {
				'authorization': `bearer ${token}`
			}
		});
		let userData = await userResponse.json();
		return (
			`
				<h2>More User Info:</h2>
				<p>Email: ${userData.email}</p>
				<p>Mobile Number: ${userData.mobileNo}</p>
			`
		);
	} catch(err) {
		console.error(err);
		alert('Something went wrong, please refresh the page and try again')
	}
}

const displayUserName = async () => {
	try {
		let userResponse = await fetch('http://localhost:3000/api/users/details', {
			'headers': {
				'authorization': `bearer ${token}`
			}
		});
		let userData = await userResponse.json();
		let nameSpan = document.querySelector('#nameSpan');
		nameSpan.innerHTML = `${userData.firstName}`
	} catch(err) {
		console.error(err);
		alert('Something went wrong, please refresh the page and try again')
	}
}

const displayProfileInfo = async () => {
	await displayUserName();
	let coursesInfo = await getEnrolledCourses();
	let userInfo = await getUserProfileInfo();
	let infoButton = document.querySelector('#infoButton');
	let infoDisplay = document.querySelector('#infoDisplay');

	infoDisplay.innerHTML = userInfo;
	infoButton.addEventListener('click', () => {
		if (infoDisplay.innerHTML === userInfo) {
			infoDisplay.innerHTML = coursesInfo;
			infoButton.innerHTML = 'Show More User Info';
		} else {
			infoDisplay.innerHTML = userInfo;
			infoButton.innerHTML = 'Show Enrolled Courses';
		}
	})
}

if (!token) {
	alert('Please login first');
	window.location.replace('./login.html');
} else {
	displayProfileInfo();
}
