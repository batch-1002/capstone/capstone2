let isUser = !!(localStorage.getItem('token'));
let token = localStorage.getItem('token');
let isAdminUser = localStorage.getItem('isAdmin') === "true";

const handleToggleActive = () => {
	let coursesContainer = document.querySelector('#coursesContainer');
	
	coursesContainer.addEventListener('click',  async (e) => {
		if (e.target && e.target.className.includes('activeButton')) {

			e.stopImmediatePropagation();

			let courseId = e.target.id.slice(2);
			let toggleActiveButton = document.querySelector(`#id${courseId}`);

			if (toggleActiveButton.innerHTML === "Active") {
				toggleActiveButton.innerHTML = '<span class="spinner-border text-primary"></span>'
				toggleActiveButton.className = "btn btn-block btn-danger";

				await fetch(`http://localhost:3000/api/courses/${courseId}`, {
					'method': 'DELETE',
					'headers': {
						'Authorization': `bearer ${token}`
					}
				})

				toggleActiveButton.className = "btn btn-block btn-danger activeButton";
				toggleActiveButton.innerHTML = "Inactive"
			} else {
				toggleActiveButton.innerHTML = '<span class="spinner-border text-primary"></span>'
				toggleActiveButton.className = "btn btn-block btn-success";
				await fetch(`http://localhost:3000/api/courses/${courseId}/make-active`, {
					'method': 'PUT',
					'headers': {
						'Authorization': `bearer ${token}`
					}
				})

				toggleActiveButton.className = "btn btn-block btn-success activeButton";
				toggleActiveButton.innerHTML = 'Active';
			}
		}
	})
}

const createToggleActiveButton = (courseData) => {
	let toggleActiveButton = document.createElement("button");
	toggleActiveButton.id = `id${courseData._id}`;
	if (courseData.isActive) {
		toggleActiveButton.className = "btn btn-block btn-success activeButton";
		toggleActiveButton.innerHTML = 'Active';
	} else {
		toggleActiveButton.className = "btn btn-block btn-danger activeButton";
		toggleActiveButton.innerHTML = 'Inactive';
	}
	return toggleActiveButton;
}

const loadCourses = async () => {
	let courseDataRepresentation;

	if (isAdminUser) {

		// fetch all courses
		try {
			let courseResponse = await fetch('http://localhost:3000/api/courses/all', {
				'method': 'GET',
				'headers': {
					'authorization': `bearer ${token}`
				}
			});
			let courseData = await courseResponse.json()
			courseDataRepresentation = convertCoursesToHTML(courseData, true);
		} catch (err) {
			alert('Something went wrong, please reload the page');
			console.error(err);
		}
	} else {
		try {
			let courseResponse = await fetch('http://localhost:3000/api/courses/', {
				'method': 'GET',
				'headers': {
					'authorization': `bearer ${token}`
				}
			});
			let courseData = await courseResponse.json();
			courseDataRepresentation = convertCoursesToHTML(courseData, false);
		} catch (err) {
			alert('Something went wrong, please reload the page');
			console.error(err);
		}
	}
}

const convertCoursesToHTML = (coursesData, isAdmin) => {
	if (coursesData.length < 1) return 'No courses currently available';
	let coursesContainer = document.querySelector('#coursesContainer');
	coursesContainer.innerHTML = '';

	coursesData.forEach(course => {
		let footer = '';
		if (isAdmin) {
			footer =
			`
				<a href="./editCourse.html?courseId=${course._id}"
					value=${course._id} class="btn-primary	btn text-white btn-block editButton">
					Edit
				</a>

			`
				// <a href="./deleteCourse.html?courseId=${course._id}"
				// 	value=${course._id} class="btn-primary	btn text-white btn-block dangerButton">
				// 	Delete
				// </a>
		} 

		footer += 
		`
			<a href="./course.html?courseId=${course._id}"
				value=${course._id} class="btn-primary	btn text-white btn-block editButton">
				Select Course
			</a>

		`

		let cardDiv = document.createElement('div');
		cardDiv.className = "col-md-6 my-3";
		let cardMain = document.createElement('div');
		cardMain.className = "card";
		let cardBody = document.createElement('div');
		cardBody.className = "card-body";

		let isEnrolled = (!isAdmin) && (course.isEnrolled);
		let enrolledMessage = (isEnrolled) ? '<em class="text-success">(Enrolled)</em>' : '';

		if (isAdmin) {
			cardBody.innerHTML = 
			`
				<div class="card-body">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">${course.description}</p>
					<p class="card-text text-left">Number of enrollees: ${course.enrollees.length}</p>
					<p class="card-text text-right">${course.price}</p>
				</div>
			`
		} else {
			cardBody.innerHTML = 
			`
				<div class="card-body">
					<h5 class="card-title">${course.name} ${enrolledMessage}</h5>
					<p class="card-text text-left">${course.description}</p>
					<p class="card-text text-right">${course.price}</p>
				</div>
			`
		}
		
		let cardFooter = document.createElement('div');
		cardFooter.className = "card-footer";
		if (isAdmin) cardFooter.appendChild(createToggleActiveButton(course));
		cardFooter.innerHTML += footer


		cardDiv.appendChild(cardMain);
		cardMain.appendChild(cardBody);
		cardMain.appendChild(cardFooter);
		handleToggleActive();
		coursesContainer.appendChild(cardDiv);
	});
}

const initializeModalButton = () => {
	let modalButton = document.querySelector('#adminButton');
	modalButton.innerHTML = 
		`
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		`
}

const initializeActiveViewButton = () => {
	let activeViewButton = document.querySelector('#activeViewButton');
	activeViewButton.innerHTML =
		`
			<button class="btn btn-block btn-info">Showing All</button>
		`
	activeViewButton2 = document.querySelector('#activeViewButton button');

	activeViewButton2.addEventListener('click', (e) => {
		if (activeViewButton.innerHTML.includes('Showing All')) {
			activeViewButton.innerHTML =
				`
					<button class="btn btn-block btn-success">Showing Active</button>
				`
		} else if (activeViewButton.innerHTML.includes('Showing Active')) {
			activeViewButton.innerHTML =
				`
					<button class="btn btn-block btn-danger">Showing Inactive</button>
				`
		} else   {
			activeViewButton.innerHTML =
				`
					<button class="btn btn-block btn-info">Showing All</button>
				`
		}
	})
}


if (isAdminUser) {
	initializeModalButton();
	// initializeActiveViewButton();
}

if (!isUser) {
	alert('Please login first');
	window.location.replace('./login.html');
} else {
	loadCourses();
}

