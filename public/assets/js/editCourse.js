let windowParams = new URLSearchParams(window.location.search);
let courseId = windowParams.get('courseId');
let token = localStorage.getItem('token');

let editForm = document.querySelector('#editCourse');
let courseNameInput = document.querySelector('#courseName');
let coursePriceInput = document.querySelector('#coursePrice');
let courseDescriptionInput = document.querySelector('#courseDescription');

editForm.addEventListener('submit', async (e) => {
	e.preventDefault();
	let updatedDetails = {
		name: courseNameInput.value,
		price: coursePriceInput.value,
		description: courseDescriptionInput.value
	}

	let updateResponse = await fetch(`http://localhost:3000/api/courses/${courseId}`, {
		'method': 'PUT',
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': `bearer ${token}`
		},
		'body': JSON.stringify(updatedDetails)
	});
	if (updateResponse) {
		alert('Course updated');
		window.location.replace('./courses.html');
	} else {
		alert('Something went wrong, refresh and try again');
	}
});

let initializeEditDisplay = async () => {
	let courseDetailResponse = await fetch(`http://localhost:3000/api/courses/${courseId}/force-get`, {
		'headers': {
			'Authorization': `bearer ${token}`
		}
	})

	let courseDetailData = await courseDetailResponse.json();
	courseNameInput.value = courseDetailData.name;
	coursePriceInput.value = courseDetailData.price;
	courseDescriptionInput.value = courseDetailData.description;
}

initializeEditDisplay();