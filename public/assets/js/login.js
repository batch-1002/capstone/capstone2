let loginForm = document.querySelector('#logInUser');

loginForm.addEventListener('submit', async (e) => {
	e.preventDefault();
	let email = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;

	if (email === '' || password === '') {
		return alert('Please input both email and password');
	}

	try {
		let loginResponse = await fetch('http://localhost:3000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email,
				password
			})
		});

		let loginData = await loginResponse.json();
		if (loginData === false) return alert('Incorrect credentials, please try again');

		localStorage.setItem('token', loginData.accessToken);

		let userDetailResponse = await fetch('http://localhost:3000/api/users/details', {
			method: 'GET',
			headers: {
				'Authorization': `bearer ${localStorage.getItem('token')}`
			}
		});

		let userDetailData = await userDetailResponse.json();
		localStorage.setItem('isAdmin', userDetailData.isAdmin);
		window.location.replace('./courses.html');
	} catch(err) {
		alert('Something went wrong please try again');
		console.error(err);
	}
})