let token = localStorage.getItem('token');
let createForm = document.querySelector('#createCourse');

createForm.addEventListener('submit', async (e) => {
	e.preventDefault();
	let courseName = document.querySelector('#courseName').value;
	let courseDescription = document.querySelector('#courseDescription').value;
	let coursePrice = document.querySelector('#coursePrice').value;
	if (courseName === '' || courseDescription === '' || coursePrice === '') {
		return alert('Please fill out all text boxes');
	}
	let creationResponse = await fetch('http://localhost:3000/api/courses', {
		'method': 'POST',
		'headers': {
			'Content-Type': 'application/json',
			'Authorization': `bearer ${token}`
		},
		'body': JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	let creationData = await creationResponse.json();
	if (creationData) {
		alert('Course successfully created');
		location.reload();
	} else {
		alert('Course with that name already exists, try another one');
	}
})